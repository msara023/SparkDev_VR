// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VREXPANSIONPLUGIN_AnimNode_ApplyHandDeltas_generated_h
#error "AnimNode_ApplyHandDeltas.generated.h already included, missing '#pragma once' in AnimNode_ApplyHandDeltas.h"
#endif
#define VREXPANSIONPLUGIN_AnimNode_ApplyHandDeltas_generated_h

#define HostProject_Plugins_VRExpansionPlugin_Source_VRExpansionPlugin_Public_Grippables_AnimNode_ApplyHandDeltas_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAnimNode_ApplyHandDeltas_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FAnimNode_Base Super;


template<> VREXPANSIONPLUGIN_API UScriptStruct* StaticStruct<struct FAnimNode_ApplyHandDeltas>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_VRExpansionPlugin_Source_VRExpansionPlugin_Public_Grippables_AnimNode_ApplyHandDeltas_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
